package com.payworks.spring.repository;

import com.payworks.spring.model.Hero;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HeroRepository extends CrudRepository<Hero, Long> {
    Hero findByName(String name);
}
