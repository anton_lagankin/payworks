package com.payworks.spring.repository;

import com.payworks.spring.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUserName(String userName);
    User findByTokens(String token);
}
