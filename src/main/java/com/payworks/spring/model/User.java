package com.payworks.spring.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"user_name"})
},
indexes = {
    @Index(name = "NAME_INDEX", columnList = "user_name")
})
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long userId;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "password")
    private String password;

    @Column(name = "non_expired")
    private boolean accountNonExpired;

    @Column(name = "non_locked")
    private boolean accountNonLocked;

    @Column(name = "credentials_non_expired")
    private boolean credentialsNonExpired;

    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "tokens")
    @ElementCollection(targetClass = String.class)
    @CollectionTable(name = "tokens",
        uniqueConstraints = {@UniqueConstraint(columnNames = "tokens")},
        indexes = {@Index(name = "TOKEN_INDEX", columnList = "tokens")}
    )
    private Set<String> tokens;

    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;

        accountNonExpired = true;
        accountNonLocked = true;
        credentialsNonExpired = true;
        enabled = true;

        tokens = new HashSet<>();
    }

    public User() {
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userName;
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<String> getTokens() {
        return Collections.unmodifiableSet(tokens);
    }

    public void addToken(String token) {
        tokens.add(token);
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setTokens(Set<String> tokens) {
        this.tokens = tokens;
    }
}
