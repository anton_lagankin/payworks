package com.payworks.spring.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(uniqueConstraints = {
               @UniqueConstraint(columnNames = {"name", "pseudonym"})
       },
indexes = {
        @Index(name = "NAME_INDEX", columnList = "name"),
        @Index(name = "PSEUDONYM", columnList = "pseudonym")
})
public class Hero {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long heroId;

    @Column(name = "name")
    private String name;

    @Column(name = "pseudonym")
    private String pseudonym;

    @Column(name = "publisher")
    private String publisher;

    @Column
    @ElementCollection(targetClass = String.class)
    private List<String> skills;

    @Column
    @ElementCollection(targetClass = String.class)
    private List<String> allies;

    @Column(name = "first_appearance_date")
    private Date firstAppearance;

    public Hero() {

    }

    @JsonCreator
    public Hero(@JsonProperty("name") String name,
                @JsonProperty("pseudonym") String pseudonym,
                @JsonProperty("publisher") String publiser,
                @JsonProperty("skills") List<String> skills,
                @JsonProperty("allies") List<String> allies,
                @JsonProperty("first-appearance")
                @JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = "GMT") Date firstAppearance) {
        this.name = name;
        this.pseudonym = pseudonym;
        this.publisher = publiser;
        this.skills = skills;
        this.allies = allies;
        this.firstAppearance = firstAppearance;
    }

    public String getName() {
        return name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public String getPublisher() {
        return publisher;
    }

    public List<String> getSkills() {
        return skills;
    }

    public List<String> getAllies() {
        return allies;
    }

    public Date getFirstAppearance() {
        return firstAppearance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hero hero = (Hero) o;

        if (allies != null ? !allies.equals(hero.allies) : hero.allies != null) return false;
        if (firstAppearance != null ? !firstAppearance.equals(hero.firstAppearance) : hero.firstAppearance != null)
            return false;
        if (name != null ? !name.equals(hero.name) : hero.name != null) return false;
        if (pseudonym != null ? !pseudonym.equals(hero.pseudonym) : hero.pseudonym != null) return false;
        if (publisher != null ? !publisher.equals(hero.publisher) : hero.publisher != null) return false;
        if (skills != null ? !skills.equals(hero.skills) : hero.skills != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (pseudonym != null ? pseudonym.hashCode() : 0);
        result = 31 * result + (publisher != null ? publisher.hashCode() : 0);
        result = 31 * result + (skills != null ? skills.hashCode() : 0);
        result = 31 * result + (allies != null ? allies.hashCode() : 0);
        result = 31 * result + (firstAppearance != null ? firstAppearance.hashCode() : 0);
        return result;
    }
}
