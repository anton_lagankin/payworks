package com.payworks.spring.security;

import com.payworks.spring.model.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Calendar;
import java.util.Date;

public class TokenHandler {
    private final String secret;
    private final UserDetailsService userService;

    public TokenHandler(String secret, UserDetailsService userService) {
        this.secret = secret;
        this.userService = userService;
    }

    public User parseUserFromToken(String token) {

        String username = Jwts.parser()
                              .setSigningKey(secret)
                              .parseClaimsJws(token)
                              .getBody()
                              .getSubject();
        return userService.loadUserByUsername(username);
    }

    public String createTokenForUser(UserDetails user) {
        Calendar expirationDate = Calendar.getInstance();
        expirationDate.add(Calendar.YEAR, 1);
        Date plusOneYeay = expirationDate.getTime();
        return Jwts.builder()
                   .setSubject(user.getUsername())
                   .setExpiration(plusOneYeay)
                   .signWith(SignatureAlgorithm.HS512, secret)
                   .compact();
    }
}
