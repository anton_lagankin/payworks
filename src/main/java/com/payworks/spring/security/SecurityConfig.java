package com.payworks.spring.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@ComponentScan({"com.payworks"})
@EnableJpaRepositories(basePackages = {"com.payworks.spring.repository", "com.payworks.spring.model"})
@EnableAutoConfiguration
@PropertySource("classpath:/security.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${secret}")
    private String secret;

    @Value("${password.hashing.key}")
    private String passwordSecret;

    @Bean
    public TokenAuthenticationService tokenAuthenticationService() {
        return new TokenAuthenticationService();
    }

    @Bean
    public StatelessAuthenticationFilter statelessAuthenticationFilter() throws Exception {
        StatelessAuthenticationFilter statelessAuthenticationFilter = new StatelessAuthenticationFilter(tokenAuthenticationService());
        statelessAuthenticationFilter.setAuthenticationManager(authenticationManagerBean());
        statelessAuthenticationFilter.setContinueChainBeforeSuccessfulAuthentication(true);
        return statelessAuthenticationFilter;
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsService();
    }

    @Bean
    public TokenHandler tokenHandler() {
        return new TokenHandler(secret, userDetailsService());
    }

    @Bean
    public PasswordHandler passwordHandler() {
        return new PasswordHandler(passwordSecret);
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean() throws Exception {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(statelessAuthenticationFilter());
        registrationBean.addUrlPatterns("/hero/**");
        registrationBean.setEnabled(true);
        return registrationBean;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.authorizeRequests()
                .antMatchers("/admin/**").permitAll();

        http.antMatcher("/hero/**")
                .addFilterBefore(statelessAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return authenticationManagerBean();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/admin/**");
    }

    @Override
    public org.springframework.security.core.userdetails.UserDetailsService userDetailsServiceBean() throws Exception {
        return new UserDetailsService();
    }
}
