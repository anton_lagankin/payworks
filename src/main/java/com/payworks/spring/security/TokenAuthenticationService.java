package com.payworks.spring.security;

import com.payworks.spring.model.User;
import com.payworks.spring.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TokenAuthenticationService {
    private static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";

    @Autowired
    private TokenHandler tokenHandler;

    @Autowired
    private UserRepository userRepository;

    public TokenAuthenticationService() {
    }

    public void addAuthentication(HttpServletResponse response, UserAuthentication authentication) {
        final UserDetails user = authentication.getDetails();
        response.addHeader(AUTH_HEADER_NAME, tokenHandler.createTokenForUser(user));
    }

    public Authentication getAuthentication(HttpServletRequest request) {
        final String token = request.getHeader(AUTH_HEADER_NAME);
        if (token == null) {
            throw new AuthenticationServiceException("No token found");
        }
        User user = userRepository.findByTokens(token);
        if (user == null) {
            throw new AuthenticationServiceException("No user for token : " + token + " found.");
        }
        User userFromToken = tokenHandler.parseUserFromToken(token);
        if (!userFromToken.getUsername().equals(user.getUsername()) ||
            !userFromToken.getPassword().equals(user.getPassword())) {
            throw new AuthenticationServiceException("No user for token : " + token + " found.");
        }
        return new UserAuthentication(user);
    }
}
