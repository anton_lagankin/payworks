package com.payworks.spring.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class PasswordHandler {

    private String secret;

    public PasswordHandler(String secret) {
        this.secret = secret;
    }

    public String createHash(String password) {
        return Jwts.builder()
                .setSubject(password)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

}
