package com.payworks.spring;

import com.payworks.controllers.AdminController;
import com.payworks.controllers.HeroController;
import org.h2.jdbcx.JdbcDataSource;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ComponentScan({"com.payworks"})
@EnableJpaRepositories(basePackages = {"com.payworks.spring.repository", "com.payworks.spring.model"})
@EnableAutoConfiguration
@PropertySource("classpath:/application.properties")
public class AppConfig {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public HeroController heroController() {
        return new HeroController();
    }

    @Bean
    public AdminController adminController() {
        return new AdminController();
    }

    @Bean
    public DataSource dataSource() {
        JdbcDataSource ds = new JdbcDataSource();
        ds.setURL("jdbc:h2:./test;AUTO_SERVER=TRUE");
        ds.setUser("sa");
        ds.setPassword("sa");
        return ds;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder) {
        Properties jpaProperties = new Properties() {
            {
                setProperty("spring.jpa.hibernate.ddl-auto", "create");
                setProperty("spring.jpa.generate-ddl", "true");
                setProperty("generateDdl", "true");
                setProperty("hibernate.hbm2ddl.auto", "update");
            }
        };

        LocalContainerEntityManagerFactoryBean bean = builder
                .dataSource(dataSource())
                .packages("com.payworks.spring.repository", "com.payworks.spring.model")
                .build();

        bean.setJpaProperties(jpaProperties);

        return bean;
    }
}
