package com.payworks.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
@SpringBootApplication
@ComponentScan("com.payworks")
public class Application {

    public static void main(String[] args) {
        ApplicationContext appContext = SpringApplication.run(Application.class, args);
    }

}
