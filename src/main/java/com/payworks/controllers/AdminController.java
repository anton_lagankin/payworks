package com.payworks.controllers;

import com.payworks.spring.model.User;
import com.payworks.spring.security.PasswordHandler;
import com.payworks.spring.security.TokenHandler;
import com.payworks.spring.security.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping(value = "admin")
public class AdminController {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private TokenHandler tokenHandler;

    @Autowired
    private PasswordHandler passwordHandler;

    public AdminController() {
        int k = 0;
    }

    @RequestMapping(value = "createUser", method = RequestMethod.PUT)
    public String createUser(@RequestParam(name = "username") String userName,
                           @RequestParam(name = "password") String password) {
        User user = new User(userName, passwordHandler.createHash(password));
        String token = tokenHandler.createTokenForUser(user);
        user.addToken(token);
        userDetailsService.addUser(user);
        return user.getTokens().iterator().next();
    }
}
