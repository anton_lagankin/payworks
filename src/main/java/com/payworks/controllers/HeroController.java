package com.payworks.controllers;

import com.payworks.spring.model.Hero;
import com.payworks.spring.repository.HeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "hero")
public class HeroController {

    @Autowired
    private HeroRepository heroRepository;

    public HeroController() {
    }

    @RequestMapping(value = "create", method = RequestMethod.PUT)
    public String createHero(@RequestBody Hero hero) {
        heroRepository.save(hero);
        return "created";
    }

    @RequestMapping(value = "find", method = RequestMethod.GET, params = "id")
    public Hero findById(@RequestParam(name = "id") long id) {
        return heroRepository.findOne(id);
    }

    @RequestMapping(value = "find", method = RequestMethod.GET, params = "name")
    public Hero findByName(@RequestParam(name = "name") String name) {
        return heroRepository.findByName(name);
    }

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public List<Hero> listAllHeroes(){
        List<Hero> result = new ArrayList<>();
        heroRepository.findAll().iterator().forEachRemaining(result::add);
        return result;
    }

}
