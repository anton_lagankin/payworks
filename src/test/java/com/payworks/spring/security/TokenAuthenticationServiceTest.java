package com.payworks.spring.security;

import com.payworks.spring.model.User;
import com.payworks.spring.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.authentication.AuthenticationServiceException;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class TokenAuthenticationServiceTest {

    @InjectMocks
    private TokenAuthenticationService tokenAuthenticationService;

    @Mock
    private TokenHandler tokenHandler;

    @Mock
    private UserRepository userRepository;

    @Mock
    private HttpServletRequest servletRequest;

    @Mock
    private User user;

    @Mock
    private User anotherUser;

    @Before
    public void init() {
        initMocks(this);
    }

    @Test(expected = AuthenticationServiceException.class)
    public void testNoAuthHeader() {
        when(servletRequest.getHeader(eq("X-AUTH-TOKEN"))).thenReturn(null);
        tokenAuthenticationService.getAuthentication(servletRequest);

        verify(servletRequest, times(1)).getHeader(eq("X-AUTH-TOKEN"));
    }

    @Test(expected = AuthenticationServiceException.class)
    public void testNoUserByToken() {
        String testToken = "test auth token";
        when(servletRequest.getHeader(eq("X-AUTH-TOKEN"))).thenReturn(testToken);
        when(userRepository.findByTokens(eq(testToken))).thenReturn(null);

        tokenAuthenticationService.getAuthentication(servletRequest);

        verify(servletRequest, times(1)).getHeader(eq("X-AUTH-TOKEN"));
        verify(userRepository, times(1)).findByTokens(eq(testToken));
    }

    @Test(expected = AuthenticationServiceException.class)
    public void testFoundTwoUsersWithDifferentUserNameAndPasswords() {
        String testToken = "test auth token";
        when(servletRequest.getHeader(eq("X-AUTH-TOKEN"))).thenReturn(testToken);
        when(userRepository.findByTokens(eq(testToken))).thenReturn(user);
        when(tokenHandler.parseUserFromToken(testToken)).thenReturn(anotherUser);

        when(user.getUsername()).thenReturn("some user name");
        when(anotherUser.getUsername()).thenReturn("some another user name");

        when(user.getPassword()).thenReturn("some password");
        when(anotherUser.getPassword()).thenReturn("some another password");

        tokenAuthenticationService.getAuthentication(servletRequest);

        verify(servletRequest, times(1)).getHeader(eq("X-AUTH-TOKEN"));
        verify(userRepository, times(1)).findByTokens(eq(testToken));
        verify(tokenHandler, times(1)).parseUserFromToken(eq(testToken));
        verify(user, times(1)).getUsername();
        verify(anotherUser, times(1)).getUsername();
        verify(user, times(1)).getPassword();
        verify(anotherUser, times(1)).getPassword();
    }

    @Test
    public void testUserFound() {
        String testToken = "test auth token";
        when(servletRequest.getHeader(eq("X-AUTH-TOKEN"))).thenReturn(testToken);
        when(userRepository.findByTokens(eq(testToken))).thenReturn(user);
        when(tokenHandler.parseUserFromToken(testToken)).thenReturn(user);
        when(user.getUsername()).thenReturn("some user name");
        when(user.getPassword()).thenReturn("some passwords");

        tokenAuthenticationService.getAuthentication(servletRequest);

        verify(servletRequest, times(1)).getHeader(eq("X-AUTH-TOKEN"));
        verify(userRepository, times(1)).findByTokens(eq(testToken));
        verify(tokenHandler, times(1)).parseUserFromToken(eq(testToken));
        verify(user, times(2)).getUsername();
        verify(user, times(2)).getPassword();
    }
}
