package com.payworks.controllers;

import com.payworks.spring.model.Hero;
import com.payworks.spring.repository.HeroRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class HeroControllerTest {

    @InjectMocks
    private HeroController heroController;

    @Mock
    private HeroRepository heroRepository;

    @Before
    public void init() {
        initMocks(this);
    }

    @Test
    public void test() throws ParseException {
        Hero hero = buildBatman();
        when(heroRepository.save(hero)).thenReturn(hero);
        heroController.createHero(hero);
        verify(heroRepository, times(1)).save(eq(hero));
    }

    @Test
    public void testFind() throws ParseException {
        Hero batman = buildBatman();
        long someId = 231;
        when(heroRepository.findOne(eq(someId))).thenReturn(batman);
        Hero actualHero = heroController.findById(someId);
        verify(heroRepository, times(1)).findOne(eq(someId));
        assertEquals(batman, actualHero);
    }

    @Test
    public void testFindByName() throws ParseException {
        Hero batman = buildBatman();
        when(heroRepository.findByName(eq(batman.getName()))).thenReturn(batman);
        Hero actualHero = heroController.findByName(batman.getName());
        verify(heroRepository, times(1)).findByName(eq(batman.getName()));
        assertEquals(batman, actualHero);
    }

    @Test
    public void testListAllHeroes() throws ParseException {
        Hero batman = buildBatman();
        Hero catwoman = buildCatwoman();
        List<Hero> expectedHeroes = Arrays.asList(batman, catwoman);
        when(heroRepository.findAll()).thenReturn(expectedHeroes);
        List<Hero> heroes = heroController.listAllHeroes();
        verify(heroRepository, times(1)).findAll();
        assertEquals(expectedHeroes, heroes);
    }

    private Hero buildBatman() throws ParseException {
        return new Hero("Batman",
                        "Bruce Wayne",
                        "DC Comics",
                        Collections.emptyList(),
                        Arrays.asList("Robin"),
                        new SimpleDateFormat("yyyy-MM-dd").parse("1939-05-01"));
    }

    private Hero buildCatwoman() throws ParseException {
        return new Hero("Catwoman",
                        "Selina Kyle",
                        "DC Comics",
                        Collections.emptyList(),
                        Collections.emptyList(),
                        new SimpleDateFormat("yyyy-MM-dd").parse("1940-03-01"));
    }
}
